
## this block is meant to set constraints on terraform version
terraform {
    required_version = ">= 1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" //optional but recommended in prod
    }
  }
}

## provider block  meant for authentication and authorization
//
//provider "aws" {
//  region     = "us-west-2"
//  access_key = "my-access-key"
 // secret_key = "my-secret-key"
## the problem with using this block is that you will have credentials exposed and we don't need that. It will be flagged as risky and a security vulnerability.


## Second Approach: In another case, we can export of env variables. The second approach they are saved in your local machine and all that. Ti change one of the variables all you need to do is ""export AWS_REGION= "us=east-1" the downside to this apporach is that if the team is big, you all must be in the same path to run the command
# if the ide is closed, you will need to rerun the file

# provider "aws" {}

# export AWS_ACCESS_KEY_ID="anaccesskey"
# export AWS_SECRET_ACCESS_KEY="asecretkey"
# export AWS_REGION="us-west-2"
# you will need to pass all those variables needed above and run the code and the provider block will only be a line. Each time you close the terminal, you may need to reconfigure stuff again.

# aws configure --profile( to use multiple users on the cli and always add the argument --profile "profile name" at the end")

# We can use assume role as well but we will be using profile. Assume role is always recommended.

provider "aws" {
  # we add the region to this profile and where is the profile? just cat the location where the aws credetails are found ~/.aws/credentials. AWS goes there and picks the default profile which is already saved with your access key and secret access key.
  region = "us-east-1"
  profile = "default" //in our .aws/credential file the prfile name is default so we use default. if i had another name, we will have used that name.
}

# Resource block is for the actual resources.
// what if the resource already exists when you are creating// We use data source to pull down available resources in AWS
// creating vpc
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16" //what happens if i want to change this value
  instance_tenancy = "default"

  tags = {
    Name = "main" //tags are the name of the resource//
  }
}


// creating ec2 instance
        // (local name) (resource name) You are allowed to change the resource name
resource "aws_instance" "test_instance" {
  ami           = var.ami_id # us-west-1 what is i want to use a windows ami
  instance_type = "t2.micro"

  tags = {
    Name = "test_instance"
  }
}